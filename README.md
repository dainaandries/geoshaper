# geoshaper

`geoshaper` builds on the features of the R packages `shiny`, `leaflet`, and `leaflet.extras`.

## Installation

```
devtools::install_git('https://gitlab.com/dainaandries/geoshaper.git')
```

## Attributions 

Code for `findLocations` function based on [StackOverflow solution](https://stackoverflow.com/questions/42528400/plot-brushing-or-accessing-drawn-shape-geometry-for-spatial-subsets-in-shiny-lea) to question "Plot brushing or accessing drawn shape geometry for spatial subsets in Shiny Leaflet" , posted March 1, 2017.