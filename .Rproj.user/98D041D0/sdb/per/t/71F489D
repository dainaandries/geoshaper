{
    "collab_server" : "",
    "contents" : "# You can learn more about package authoring with RStudio at:\n#\n#   http://r-pkgs.had.co.nz/\n#\n# Some useful keyboard shortcuts for package authoring:\n#\n#   Build and Reload Package:  'Cmd + Shift + B'\n#   Check Package:             'Cmd + Shift + E'\n#   Test Package:              'Cmd + Shift + T'\n\n#' Find locations inside a polygon, square, or circle drawn with leaflet.extras drawing tools on a Shiny Leaflet map.\n#'\n#' @param shape Shiny input (input$MAPID_draw_new_feature), representing shape drawn on the map by the user.\n#' @param location_coordinates A SpatialPointsDataFrame containing coordinates and ids for all map locations.\n#' @param location_id_colname Column name from location_coordinates containing desired names or ids for set of locations returned.\n#' @return A vector of location ids.\n#' @examples\n#' mock_input.map_feature <- list(type = \"Feature\"\n#'                          , properties = list(`_leaflet_id`= 13477, feature_type = \"rectangle\")\n#'                          , geometry = list(type = \"Polygon\"\n#'                          , coordinates = list(list(list(-76.15723, 39.51252)\n#'                          , list(-76.15723,  40.30467), list(-74.73999, 40.30467)\n#'                          , list(-74.73999, 39.51252), list(-76.15723, 39.51252)))))\n#' airports <- data.frame('locationID' = c('PHL', 'DTW')\n#'                       , 'Longitude' = c(-75.2408, -83.3533)\n#'                       , 'Latitude' = c(39.8722, 42.2125))\n#' coords = sp::SpatialPointsDataFrame(airports[,c('Longitude', 'Latitude')], airports)\n#' findLocations(shape = mock_input.map_feature\n#'                      , location_coordinates = coords\n#'                      , location_id_colname = \"locationID\")\n\n\nfindLocations <- function(shape, location_coordinates, location_id_colname) {\n\n  # derive polygon coordinates and feature_type from shape input\n  polygon_coordinates <- shape$geometry$coordinates\n  feature_type <- shape$properties$feature_type\n\n  if(feature_type %in% c(\"rectangle\",\"polygon\")) {\n\n    # transform into a spatial polygon\n    drawn_polygon <- sp::Polygon(do.call(rbind,lapply(polygon_coordinates[[1]],function(x){c(x[[1]][1],x[[2]][1])})))\n\n    # identify selected locations\n    selected_locs <- sp::over(location_coordinates, sp::SpatialPolygons(list(sp::Polygons(list(drawn_polygon),\"drawn_polygon\"))))\n\n    # get location ids\n    x = (location_coordinates[which(!is.na(selected_locs)), location_id_colname])\n\n    selected_loc_id = as.character(x[[location_id_colname]])\n\n    return(selected_loc_id)\n\n  } else if (feature_type == \"circle\") {\n\n    center_coords <- matrix(c(polygon_coordinates[[1]], polygon_coordinates[[2]])\n                            , ncol = 2)\n\n    # get distances to center of drawn circle for all locations in location_coordinates\n    # distance is in kilometers\n    dist_to_center <- sp::spDistsN1(location_coordinates, center_coords, longlat = TRUE)\n\n    # get location ids\n    # radius is in meters\n    x <- location_coordinates[dist_to_center < shape$properties$radius/1000, location_id_colname]\n\n    selected_loc_id = as.character(x[[location_id_colname]])\n\n    return(selected_loc_id)\n  }\n}\n",
    "created" : 1508789573132.000,
    "dirty" : false,
    "encoding" : "UTF-8",
    "folds" : "",
    "hash" : "2684659582",
    "id" : "71F489D",
    "lastKnownWriteTime" : 1509127493,
    "last_content_update" : 1509127493831,
    "path" : "~/Documents/geoshaper/R/findLocations.R",
    "project_path" : "R/findLocations.R",
    "properties" : {
        "tempName" : "Untitled1"
    },
    "relative_order" : 3,
    "source_on_save" : false,
    "source_window" : "",
    "type" : "r_source"
}